#include<stdio.h>
#include<libsecret/secret.h>

const SecretSchema * myschema_get(void) G_GNUC_CONST;
#define CHROMESCHEMA chromeschema_get ()

const SecretSchema *
chromeschema_get(void) {
	static const SecretSchema the_schema = {
		"chrome_libsecret_os_crypt_password_v2", SECRET_SCHEMA_DONT_MATCH_NAME,
		{
			{"application", SECRET_SCHEMA_ATTRIBUTE_STRING},
			{NULL, SECRET_SCHEMA_ATTRIBUTE_STRING}
		}
	};
	return &the_schema;
}

int main(void) {
	GError *error = NULL;
	// lookup
	gchar *password = secret_password_lookup_sync(CHROMESCHEMA, NULL, &error,
			"application", "chrome",
			NULL);
	if(error != NULL) {
		puts("there was some sort of error");
		g_error_free(error);
	} else if(password == NULL) {
		puts("no matches bro");
	} else {
		printf("the password is: %s\n", password);
		secret_password_free(password);
	}
	return 0;
}

